const express = require('express');
const router = express.Router();

module.exports = (users) => {
    router.get('/time', (req, res) => {
        const { currentTime } = req.cookies;
        if (!currentTime) {
            res.status(403).json({
                message: 'current time is not exist in cookies'
            })
            return
        }
    
        res.status(200).json({
            time: currentTime
        });
    });
    
    
    router.route('/users')
        .get((req, res) => {
            res.status(200).json({
                users
            })
        })
        .post((req, res) => {
            const { username, password, gender, agree = 'off' } = req.body;
            if(!username && !password && !gender) {
                res.status(503).json({
                    message: 'some fields are empty'
                })
                return
            }
            users.push({ username, password, gender, agree });
            res.status(200).json({
                users
            })
        })

    return router
}