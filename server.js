const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser')
const moment = require('moment');

const config = require('./config');
const { getViewPath } = require('./helpers');

// api router
const api = require('./api');

global.users = [];

const app = express();

app.set('view engine', 'pug');

app.use(cookieParser());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse application/json
app.use(bodyParser.json())

app.use('/', (req, res, next) => {
    const { currentTime } = req.cookies
    if(!currentTime) {
        const currentTime = moment().format('HH:mm:ss');
        res.cookie('currentTime', currentTime)
    }
    next();
});

app.use(`${config.api}`, api(users));

app.get('/', (req, res) => res.send('Hello World!'));
app.get('/form', (req, res) => {
    res.render(getViewPath('form'))
});
app.get('/result', (req, res) => {
    res.render(getViewPath('result'), users)
});
app.get('/myroute/:param', (req, res) => {
    const { 
        cookies: {currentTime},
        params: {param},
        query,
        headers
    } = req

    res.render(getViewPath('myroute'), { currentTime, param, query, headers })
});

app.post('/form', (req, res) => {
    const { username, password, gender, agree = false } = req.body;
    if(!username && !password && !gender) {
        return
    }
    users.push({ username, password, gender, agree: agree === 'on' ? true : false });
    res.redirect('/result')
});


app.listen(config.port, () => console.log(`App listening on port ${config.port}!`));
