const path = require('path');

exports.getViewPath = (viewName) => {
    return path.join(__dirname, `../views/${viewName}.pug`)
}
